# ShareTV Movie Insert

`Before inserting a new movie, try to find it to avoid duplicates`
1. Use this HTML only for  `movie insert`
2. Supported sites [`Rapidvideo`, `Streamango`, `Openload`] (Please try to insert only `Rapidvideo` or `Streamango` links, it is the most stable and fast server)
3. Change uploder nickname in HTML code
4. Please, report any error

Sample Links

1. Rapidvideo: `https://www.rapidvideo.com/v/FVEEJ6B93J` (Dont use /embed/ or other)
2. Streamango: `https://streamango.com/f/ftsalroeckklsakb` (Dont use /embed/ or other)
